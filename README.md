# myrstack

Generates maven pom.xml files from YAML files.

## Why

The reason is that creating pom.xml files for a multimodule project can be a little bit daunting for someone new to Maven. Myrstack creates the pom files from YAML files which are a little bit easier to read and create. The goal is to be a one-time scaffolding to create the pom files.

Myrstack, like maven, is opinionated. Meaning that myrstack will set up the project in one way and that way is the only one. When myrstack is finished you can edit the pom files as you like. 

## How to run

Myrstack is a java application and requires JDK 11 or later.
