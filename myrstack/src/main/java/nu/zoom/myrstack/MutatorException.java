package nu.zoom.myrstack;

public class MutatorException extends Exception {
    public MutatorException(String message) {
        super(message);
    }

    public MutatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public MutatorException(Throwable cause) {
        super(cause);
    }
}
