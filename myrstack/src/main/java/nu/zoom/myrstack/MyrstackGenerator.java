package nu.zoom.myrstack;

import nu.zoom.myrstack.dst.MavenProjectBuilder;
import nu.zoom.myrstack.src.DirectoryWalker;
import nu.zoom.myrstack.src.MyrstackProjectFileNode;
import nu.zoom.myrstack.src.ProjectFileNotFoundException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class MyrstackGenerator {
    private final MyrstackLogger logger;
    private final Path projectRootDirectory;

    public MyrstackGenerator(MyrstackLogger logger, Path projectRootDirectory) {
        this.logger = logger;
        this.projectRootDirectory = projectRootDirectory.normalize().toAbsolutePath();
    }

    void generate() throws IOException, ProjectFileNotFoundException, MutatorException {

        final DirectoryWalker walker = new DirectoryWalker(this.logger, this.projectRootDirectory);
        final MyrstackProjectFileNode myrstackProjectFileNode = walker.buildSourceTree();
        final MyrstackProjectModelBuilder mpmBuilder = new MyrstackProjectModelBuilder(this.logger);
        final MutableProjectNode rootNode = mpmBuilder.buildFromSource(myrstackProjectFileNode);

        final List<ModelMutator> mutators = List.of(
                new PruneEmptyBranchesMutator(this.logger),
                new PopulateFromYAMLMutator(this.logger),
                new NodeTypeMutator(this.logger),
                new GroupArtifactVersionMutator(this.logger)
        );
        for (ModelMutator mutator : mutators) {
            mutator.mutate(rootNode);
        }

        MavenProjectBuilder pb = null;
        try {
            pb = new MavenProjectBuilder(this.logger);
            pb.generatePOMs(rootNode);
        } catch (JAXBException | ParserConfigurationException e) {
            throw new MutatorException(e);
        }
    }
}
