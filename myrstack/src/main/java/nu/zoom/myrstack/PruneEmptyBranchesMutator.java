package nu.zoom.myrstack;

public class PruneEmptyBranchesMutator extends LoggingMutator {
    public PruneEmptyBranchesMutator(MyrstackLogger logger) {
        super(logger);
    }

    @Override
    public void mutate(MutableProjectNode rootNode) throws MutatorException {
        boolean mpmDirty;

        do {
            mpmDirty = depthFirstUnlinkEmptyNode(rootNode);
        } while (mpmDirty);
    }

    private boolean depthFirstUnlinkEmptyNode(MutableProjectNode node) {
        if (node.hasChildren()) {
            // recurse
            for (MutableProjectNode child : node.getChildren()) {
                if (depthFirstUnlinkEmptyNode(child)) {
                    return true;
                }
            }
        } else {
            if (!node.getProjectFileNode().getProjectFile().isPresent()) {
                // node without children and without project file.
                logger.debug("Unlinking " + node.getProjectFileNode().getPath());
                node.getParentNode().unlinkChild(node);
                return true;
            }
        }

        return false;
    }
}
