package nu.zoom.myrstack.src;

import java.nio.file.Path;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class MyrstackProjectFileNode {
    private Set<MyrstackProjectFileNode> children = Collections.emptySet();

    private final Path path ;

    // At least one of these must be present
    private Optional<Path> projectFile;
    private Optional<MyrstackProjectFileNode> parent;


    public MyrstackProjectFileNode(Path path, Path projectFile) {
        this.path = Objects.requireNonNull(path) ;
        this.parent = Optional.empty();
        this.projectFile = Optional.of(projectFile);
    }

    public MyrstackProjectFileNode(Path path, MyrstackProjectFileNode parent) {
        this.path = Objects.requireNonNull(path) ;
        this.parent = Optional.of(parent);
        this.projectFile = Optional.empty();

    }

    public MyrstackProjectFileNode(Path path, MyrstackProjectFileNode parent, Path projectFile) {
        this.path = Objects.requireNonNull(path) ;
        this.parent = Optional.of(parent);
        this.projectFile = Optional.of(projectFile);
    }

    public Optional<Path> getProjectFile() {
        return projectFile;
    }

    public Optional<MyrstackProjectFileNode> getParent() {
        return parent;
    }

    public Set<MyrstackProjectFileNode> getChildren() {
        return children;
    }

    void setChildren(Set<MyrstackProjectFileNode> children) {
        this.children = children;
    }

    public Path getPath() {
        return path;
    }
}
