package nu.zoom.myrstack.src;

import nu.zoom.myrstack.MyrstackLogger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DirectoryWalker {
    private final Path rootDirectory;
    private final MyrstackLogger logger;

    public DirectoryWalker(final MyrstackLogger logger, final Path rootDirectory) {
        this.rootDirectory = Objects.requireNonNull(rootDirectory);
        this.logger = Objects.requireNonNull(logger);
    }

    public MyrstackProjectFileNode buildSourceTree() throws ProjectFileNotFoundException, IOException {
        final Path rootProjectSrcFile = findMyrstackProjectFile(this.rootDirectory)
                .orElseThrow(() -> new ProjectFileNotFoundException(this.rootDirectory));
        final MyrstackProjectFileNode rootNode = new MyrstackProjectFileNode(rootDirectory, rootProjectSrcFile);
        visitSubdirectories(this.rootDirectory, rootNode);
        return rootNode;
    }

    private void visitSubdirectories(final Path directory, MyrstackProjectFileNode node) throws IOException {
        final Set<MyrstackProjectFileNode> childNodes = Files.list(directory)
                .filter(p -> Files.isDirectory(p))
                .map(p -> buildNodeForPath(p, node))
                .collect(Collectors.toSet());
        if (!childNodes.isEmpty()) {
            node.setChildren(childNodes);

            // recurse
            for (MyrstackProjectFileNode childNode : childNodes) {
                visitSubdirectories(childNode.getPath(), childNode);
            }
        }
    }

    private MyrstackProjectFileNode buildNodeForPath(Path path, MyrstackProjectFileNode parentNode) {
        final Optional<Path> myrstackProjectFile = findMyrstackProjectFile(path);
        logger.debug(String.format(
                "Visiting: %s, contains project file: %s",
                path.toString(),
                (myrstackProjectFile.isPresent() ? "true" : "false"))
        );
        final MyrstackProjectFileNode node = myrstackProjectFile
                .map(pf -> new MyrstackProjectFileNode(path, parentNode, pf))
                .orElse(new MyrstackProjectFileNode(path, parentNode));
        return node;
    }

    private Optional<Path> findMyrstackProjectFile(Path directory) {
        return Stream.of("myrstack.yml", "myrstack.yaml")
                .map(s -> directory.resolve(s))
                .filter(s -> isProjectFile(directory.resolve(s)))
                .findFirst();
    }

    private boolean isProjectFile(final Path p) {
        return (Files.isReadable(p) && Files.isRegularFile(p));
    }
}
