package nu.zoom.myrstack.src;

import java.nio.file.Path;

public class ProjectFileNotFoundException extends Exception {

    public ProjectFileNotFoundException(Path path) {
        super("Unable to find myrstack.yml project file in directory: " + path.normalize().toString());
    }
}
