package nu.zoom.myrstack;

import picocli.CommandLine;

public class Myrstack {
    public static void main(String... args){
        int exitCode = new CommandLine(new Launcher()).execute(args) ;
        System.exit(exitCode);
    }
}
