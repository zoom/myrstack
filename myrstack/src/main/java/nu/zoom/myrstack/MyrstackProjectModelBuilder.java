package nu.zoom.myrstack;

import nu.zoom.myrstack.src.MyrstackProjectFileNode;

public class MyrstackProjectModelBuilder {
    private final MyrstackLogger logger;

    public MyrstackProjectModelBuilder(MyrstackLogger logger) {
        this.logger = logger;
    }

    public MutableProjectNode buildFromSource(MyrstackProjectFileNode rootSourceNode) {
        MutableProjectNode rootNode = new MutableProjectNode(rootSourceNode, null);
        buildChildProjectNodes(rootNode);
        return rootNode ;
    }

    private void buildChildProjectNodes(MutableProjectNode node) {
        node.getProjectFileNode()
                .getChildren()
                .stream()
                .map(fn -> new MutableProjectNode(fn, node))
                .forEach(cn -> node.addChild(cn));

        // recurse
        for (MutableProjectNode child : node.getChildren()) {
            buildChildProjectNodes(child);
        }
    }
}
