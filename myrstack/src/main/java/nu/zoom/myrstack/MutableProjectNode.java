package nu.zoom.myrstack;

import nu.zoom.myrstack.src.MyrstackProjectFileNode;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class MutableProjectNode {
    private MutableProjectNode parentNode;
    private final MyrstackProjectFileNode projectFileNode;
    private Set<MutableProjectNode> children = new HashSet<>();
    private String groupID;
    private String artifactID;
    private String version ;
    private ModelNodeType modelNodeType ;

    public MutableProjectNode(MyrstackProjectFileNode projectFileNode, MutableProjectNode parent) {
        this.projectFileNode = Objects.requireNonNull(projectFileNode);
        this.parentNode = parent;
    }

    public MyrstackProjectFileNode getProjectFileNode() {
        return projectFileNode;
    }

    public void unlinkChild(MutableProjectNode childNode) {
        this.children.remove(childNode);
        childNode.parentNode = null;
    }

    public void addChild(MutableProjectNode child) {
        this.children.add(child);
    }

    public Set<MutableProjectNode> getChildren() {
        return children;
    }

    public boolean hasChildren() {
        return !this.children.isEmpty();
    }

    public MutableProjectNode getParentNode() {
        return parentNode;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getArtifactID() {
        return artifactID;
    }

    public void setArtifactID(String artifactID) {
        this.artifactID = artifactID;
    }

    public void setModelNodeType(ModelNodeType modelNodeType) {
        this.modelNodeType = modelNodeType;
    }

    public ModelNodeType getModelNodeType() {
        return modelNodeType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
