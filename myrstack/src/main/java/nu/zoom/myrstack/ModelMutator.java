package nu.zoom.myrstack;

@FunctionalInterface
public interface ModelMutator {
    void mutate(MutableProjectNode rootNode) throws MutatorException;
}
