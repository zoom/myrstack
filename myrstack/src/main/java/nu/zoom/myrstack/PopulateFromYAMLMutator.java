package nu.zoom.myrstack;

import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class PopulateFromYAMLMutator extends LoggingMutator {
    private final Yaml yaml;

    public PopulateFromYAMLMutator(MyrstackLogger logger) {
        super(logger);
        yaml = new Yaml();
    }

    @Override
    public void mutate(MutableProjectNode rootNode) throws MutatorException {
        try {
            handleNode(rootNode);
        } catch (IOException e) {
            throw new MutatorException(e);
        }

        final String groupID = rootNode.getGroupID();
        if (groupID == null || groupID.length() < 1) {
            throw new MutatorException("Root project must have a 'groupid' defined in myrstack.yml");
        }

        if (!groupID.contains(".")) {
            logger.info("The groupID should ideally be the reverse domain name e.g. 'com.foo.project', please verify that: '"
                    + groupID
                    + "' is the groupID you want to use.");
        }

        final String artifactID = rootNode.getArtifactID();
        if (artifactID == null || artifactID.length() < 1) {
            throw new MutatorException("Root project must have a 'artifactid' defined in myrstack.yml");
        }

        if (!rootNode.getChildren().isEmpty()) {
            if (!artifactID.endsWith("-parent")) {
                logger.info("In a multimodule project it is common to name the top level artifact <artifact>-parent. Please verify that: '"
                        + artifactID
                        + "' is the top level artifact name you want to use.");
            }
        }
    }

    private void handleNode(MutableProjectNode node) throws IOException {
        final Optional<Path> projectFile = node.getProjectFileNode().getProjectFile();
        if (projectFile.isPresent()) {
            final Path path = projectFile.get();
            this.logger.debug("Reading: " + path);
            final Object unmarshalled = yaml.load(Files.newInputStream(path));
            if (unmarshalled != null && unmarshalled instanceof Map) {

                final Object groupid = ((Map) unmarshalled).get("groupid");
                if (groupid != null) {
                    logger.debug("Group ID: " + groupid);
                    node.setGroupID(groupid.toString());
                }
                final Object artifactid = ((Map) unmarshalled).get("artifactid");
                if (artifactid != null) {
                    logger.debug("Artifact ID: " + artifactid);
                    node.setArtifactID(artifactid.toString());
                }
            }
        }
        final Set<MutableProjectNode> children = node.getChildren();
        for (MutableProjectNode child : children) {
            handleNode(child);
        }
    }
}
