package nu.zoom.myrstack;

import java.util.Set;

public class NodeTypeMutator extends LoggingMutator {
    public NodeTypeMutator(MyrstackLogger logger) {
        super(logger);
    }

    @Override
    public void mutate(MutableProjectNode rootNode) throws MutatorException {
        handleNode(rootNode);
    }

    private void handleNode(MutableProjectNode node) {
        final Set<MutableProjectNode> children = node.getChildren();
        if (children.isEmpty()) {
            node.setModelNodeType(ModelNodeType.SOURCE);
        } else {
            node.setModelNodeType(ModelNodeType.REACTOR);
            for (MutableProjectNode child : children) {
                handleNode(child);
            }
        }
    }
}
