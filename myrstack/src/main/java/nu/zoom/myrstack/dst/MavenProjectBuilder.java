package nu.zoom.myrstack.dst;

import nu.zoom.myrstack.MutableProjectNode;
import nu.zoom.myrstack.MyrstackLogger;
import org.apache.maven.pom._4_0.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

public class MavenProjectBuilder {
    private static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
    private final JAXBContext jaxbContext;
    private final MyrstackLogger logger;
    private final Marshaller marshaller;
    private final Document doc;

    public MavenProjectBuilder(MyrstackLogger logger) throws JAXBException, ParserConfigurationException {
        this.logger = logger;
        this.jaxbContext = JAXBContext.newInstance(Model.class);
        this.marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        this.doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

    }

    public void generatePOMs(MutableProjectNode rootNode) throws IOException, JAXBException {
        createPOM(rootNode, true);
    }

    void createPOM(MutableProjectNode node, boolean rootNode) throws IOException, JAXBException {
        final Model model = OBJECT_FACTORY.createModel();
        model.setArtifactId(node.getArtifactID());
        model.setGroupId(node.getGroupID());
        model.setVersion(node.getVersion());

        MutableProjectNode parentNode = node.getParentNode();
        if (parentNode != null && parentNode != node) {
            Parent parent = OBJECT_FACTORY.createParent();
            parent.setGroupId(parentNode.getGroupID());
            parent.setArtifactId(parentNode.getArtifactID());
            parent.setVersion(parentNode.getVersion());
            model.setParent(parent);
        }

        final String projectPackaging;
        switch (node.getModelNodeType()) {
            case SOURCE:
                projectPackaging = "jar";
                break;
            case REACTOR:
                projectPackaging = "pom";
                Set<MutableProjectNode> children = node.getChildren();
                if (!children.isEmpty()) {
                    Model.Modules modelModules = OBJECT_FACTORY.createModelModules();
                    for (MutableProjectNode child : children) {
                        String moduleName = child.getProjectFileNode().getPath().getFileName().toString();
                        modelModules.getModule().add(moduleName);
                        createPOM(child, false);
                    }
                    model.setModules(modelModules);
                }
                break;
            default:
                throw new RuntimeException("Unknown project model type");
        }
        model.setPackaging(projectPackaging);

        if (rootNode) {
            addRootNodeData(model);
        }

        createFile(node.getProjectFileNode().getPath(), model);
    }

    private void addRootNodeData(Model model) {
        final DependencyManagement dependencyManagement = OBJECT_FACTORY.createDependencyManagement();
        final DependencyManagement.Dependencies dependecies = OBJECT_FACTORY.createDependencyManagementDependencies();

        final Dependency sampleDependecy = OBJECT_FACTORY.createDependency();
        sampleDependecy.setArtifactId("junit-jupiter");
        sampleDependecy.setGroupId("org.junit.jupiter");
        sampleDependecy.setVersion("5.5.2");
        dependecies.getDependency().add(sampleDependecy);
        dependencyManagement.setDependencies(dependecies);

        model.setDependencyManagement(dependencyManagement);

        final Model.Properties modelProperties = OBJECT_FACTORY.createModelProperties();
        final List<Element> properties = modelProperties.getAny();
        properties.add(createElement("project.build.sourceEncoding", "UTF-8"));
        properties.add(createElement("maven.compiler.source", "11"));
        properties.add(createElement("maven.compiler.target", "11"));
        model.setProperties(modelProperties);
    }

    private Element createElement(final String localPart, final String value) {
        // Use same namespace as schema or the tags will have a xmlns attribute
        final Element element = doc.createElementNS("http://maven.apache.org/POM/4.0.0", localPart);
        element.setTextContent(value);
        return element;
    }

    private void createFile(Path path, Model model) throws IOException, JAXBException {
        JAXBElement<Model> project = OBJECT_FACTORY.createProject(model);
        try (OutputStream outs = Files.newOutputStream(path.resolve("pom.xml"))) {
            marshaller.marshal(project, outs);
        }
    }
}
