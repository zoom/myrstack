package nu.zoom.myrstack;

import nu.zoom.myrstack.src.DirectoryWalker;
import nu.zoom.myrstack.src.MyrstackProjectFileNode;
import nu.zoom.myrstack.src.ProjectFileNotFoundException;
import picocli.CommandLine;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "myrstack", mixinStandardHelpOptions = true,
        description = "Generate maven pom.xml files from myrstack.yml files.")
public class Launcher implements Callable<Integer> {
    private MyrstackLogger logger;

    @CommandLine.Parameters(index = "0", description = "The project root directory.")
    private File projectRootDirectory;

    @CommandLine.Option(names = {"-q", "--quiet"}, description = "Supress all output except errors on stderr.")
    private boolean quiet;

    @CommandLine.Option(names = {"-v", "--verbose"}, description = "Supply extended information logging.")
    private boolean verbose;

    @Override
    public Integer call() throws Exception {
        this.logger = new MyrstackLogger(this.quiet, this.verbose);
        if (projectRootDirectory == null || !projectRootDirectory.isDirectory()) {
            this.logger.error(
                    String.format(
                            "The project root directory: %s does not appear to be a directory.",
                            projectRootDirectory
                    )
            );
            return CommandLine.ExitCode.USAGE;
        }
        try {
            new MyrstackGenerator(logger, projectRootDirectory.toPath()).generate();
        } catch (Exception e) {
            logger.error(e.toString());
            return CommandLine.ExitCode.USAGE;
        }

        return CommandLine.ExitCode.OK;
    }
}
