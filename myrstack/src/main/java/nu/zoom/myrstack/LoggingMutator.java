package nu.zoom.myrstack;

public abstract class LoggingMutator implements ModelMutator {
    protected final MyrstackLogger logger;

    public LoggingMutator(MyrstackLogger logger) {
        this.logger = logger;
    }
}
