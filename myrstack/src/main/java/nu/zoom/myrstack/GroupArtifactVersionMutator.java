package nu.zoom.myrstack;

import java.nio.file.Path;

public class GroupArtifactVersionMutator extends LoggingMutator {

    public static final String DEFAULT_VERSION = "1.0.0-SNAPSHOT";

    public GroupArtifactVersionMutator(MyrstackLogger logger) {
        super(logger);
    }

    @Override
    public void mutate(MutableProjectNode rootNode) throws MutatorException {
        final String defaultGroupID = rootNode.getGroupID();
        if (defaultGroupID == null || defaultGroupID.length() < 1) {
            throw new MutatorException("Root node must have a group ID");
        }

        // Root node must have a version
        String version = rootNode.getVersion();
        if (version == null || version.length() < 1) {
            logger.debug("Setting root version to: " + DEFAULT_VERSION);
            rootNode.setVersion(DEFAULT_VERSION);
        }

        handleNode(rootNode);
    }

    private void handleNode(MutableProjectNode node) {
        final String groupID = node.getGroupID();
        if (groupID == null || groupID.length() < 1) {
            final String parentGroupID = getParentGroupID(node);
            logger.debug(node.getProjectFileNode().getPath() + " - Inheriting parent groupID: " + parentGroupID);
            node.setGroupID(parentGroupID);
        }

        final String artifactID = node.getArtifactID();
        if (artifactID == null || artifactID.length() < 1) {
            final Path fileName = node.getProjectFileNode().getPath().getFileName();
            final String calculatedArtifactID = fileName.toString();
            logger.debug(node.getProjectFileNode().getPath() + " - Using calculated artifactID: " + calculatedArtifactID);
            node.setArtifactID(calculatedArtifactID);
        }

        final String version = node.getVersion();
        if (version == null || version.length() < 1) {
            final String inheritedVersion = getParentVersion(node);
            logger.debug(node.getProjectFileNode().getPath() + " - Inheriting version: " + inheritedVersion);
            node.setVersion(inheritedVersion);
        }

        node.getChildren().forEach(cn -> handleNode(cn));
    }

    private String getParentGroupID(MutableProjectNode node) {
        final MutableProjectNode parentNode = node.getParentNode();
        final String parentNodeGroupID = parentNode.getGroupID();
        if (parentNodeGroupID == null || parentNodeGroupID.length() < 1) {
            return getParentGroupID(parentNode);
        } else {
            return parentNodeGroupID;
        }
    }

    private String getParentVersion(MutableProjectNode node) {
        final MutableProjectNode parentNode = node.getParentNode();
        final String parentVersion = parentNode.getVersion();
        if (parentVersion == null || parentVersion.length() < 1) {
            return getParentVersion(parentNode);
        } else {
            return parentVersion;
        }
    }
}
