package nu.zoom.myrstack;

public class MyrstackLogger {
    private final boolean quiet ;
    private final boolean verbose ;

    public MyrstackLogger(boolean quiet, boolean verbose) {
        this.quiet = quiet;
        this.verbose = verbose;
    }

    public void error(final String errorMessage) {
        System.err.println(errorMessage);
    }

    public void info(final String message) {
        if (!this.quiet) {
            System.out.println(message);
        }
    }

    public void debug(final String message) {
        if (this.verbose && !this.quiet) {
            System.out.println(message);
        }
    }

}
