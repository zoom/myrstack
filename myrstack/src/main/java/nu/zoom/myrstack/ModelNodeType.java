package nu.zoom.myrstack;

public enum ModelNodeType {
    REACTOR, SOURCE
}
