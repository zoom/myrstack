package nu.zoom.myrstack.src;

import nu.zoom.myrstack.MyrstackLogger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

class DirectoryWalkerTest {
    private MyrstackLogger logger = new MyrstackLogger(false, true);
    private Path projectRootPath;

    @BeforeEach
    void setUp() throws IOException {
        this.projectRootPath = Files.createTempDirectory("myrstacktest").normalize().toAbsolutePath();
    }

    @Test
    @DisplayName("A root directory without project file throws exception")
    void buildSourceTreeMissingProjectFile() {
        DirectoryWalker w = new DirectoryWalker(logger, projectRootPath);
        Assertions.assertThrows(ProjectFileNotFoundException.class, () -> w.buildSourceTree());
    }

    @Test
    @DisplayName("Empty directories below root are included in the tree")
    void buildSourceTreeEmptyDirectories() throws IOException, ProjectFileNotFoundException {
        MyrstackProjectFileUtils.createProjectFile(this.projectRootPath);

        final Path leaf1 = Paths.get(this.projectRootPath.toString(), "empty1", "leaf1");
        Files.createDirectories(leaf1);
        final Path leaf2 = Paths.get(this.projectRootPath.toString(), "empty2", "empty2.2", "leaf2");
        Files.createDirectories(leaf2);
        MyrstackProjectFileUtils.createProjectFile(leaf2);

        DirectoryWalker w = new DirectoryWalker(logger, projectRootPath);
        final MyrstackProjectFileNode fileNode = w.buildSourceTree();

        Assertions.assertNotNull(fileNode);

        final Set<MyrstackProjectFileNode> rootChildren = fileNode.getChildren();
        Assertions.assertNotNull(rootChildren);
        Assertions.assertFalse(rootChildren.isEmpty());

        final MyrstackProjectFileNode leaf1Node =
                MyrstackProjectFileUtils.getNodeInBranch(this.projectRootPath, fileNode, "empty1", "leaf1");
        Assertions.assertFalse(leaf1Node.getProjectFile().isPresent());

        final MyrstackProjectFileNode leaf2Node =
                MyrstackProjectFileUtils.getNodeInBranch(this.projectRootPath, fileNode, "empty2", "empty2.2", "leaf2");
        Assertions.assertTrue(leaf2Node.getProjectFile().isPresent());
    }
}