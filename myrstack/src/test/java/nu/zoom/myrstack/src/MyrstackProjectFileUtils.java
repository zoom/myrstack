package nu.zoom.myrstack.src;

import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public class MyrstackProjectFileUtils {
    static Path createProjectFile(Path root) throws IOException {
        Path path = Files.createFile(root.resolve("myrstack.yml"));
        return path;
    }

    public static MyrstackProjectFileNode getNodeInBranch(Path root, MyrstackProjectFileNode rootNode, String... parts) {
        Assertions.assertNotNull(root, "root path has a value");
        Assertions.assertNotNull(rootNode, "Root node has a value");
        Assertions.assertNotNull(parts, "Parts have a a value");
        Assertions.assertTrue(parts.length > 0, "There is at least on part");

        assertPathMatchesNode(root, rootNode);

        Path currentPath = root;
        MyrstackProjectFileNode currentNode = rootNode;
        for (String part : parts) {
            currentPath = currentPath.resolve(part);
            currentNode = findChildMatchingPath(currentPath, currentNode);
        }
        return currentNode;
    }

    private static MyrstackProjectFileNode findChildMatchingPath(Path childPath, MyrstackProjectFileNode node) {
        Assertions.assertNotNull(childPath, "child path has a value");
        Assertions.assertNotNull(node, "Node has a value");
        Optional<MyrstackProjectFileNode> childNodeMatchingPath =
                node
                        .getChildren()
                        .stream()
                        .filter(cn -> cn.getPath().equals(childPath))
                        .findFirst();
        Assertions.assertTrue(childNodeMatchingPath.isPresent());
        return childNodeMatchingPath.get();
    }

    public static void assertPathMatchesNode(Path expected, MyrstackProjectFileNode actual) {
        Assertions.assertNotNull(actual, "Node has a value");
        Assertions.assertNotNull(actual.getPath(), "Node path has a value");
        Assertions.assertSame(expected, actual.getPath());
    }
}
