package nu.zoom.myrstack;


import nu.zoom.myrstack.src.MyrstackProjectFileNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

class MyrstackProjectModelBuilderTest {
    private MyrstackLogger logger = new MyrstackLogger(false, true);

    @Test
    @DisplayName("Build a model from a single project file node")
    void buildSimpleFromSource() {
        MyrstackProjectFileNode rootNode = createFakeFileRootNode();
        MyrstackProjectModelBuilder projectModelBuilder = new MyrstackProjectModelBuilder(logger);

        final MutableProjectNode projectRootNode = projectModelBuilder.buildFromSource(rootNode);
        Assertions.assertNotNull(projectRootNode);

        final MyrstackProjectFileNode rootFileNode = projectRootNode.getProjectFileNode();
        Assertions.assertNotNull(rootFileNode);
        Assertions.assertTrue(rootFileNode.getProjectFile().isPresent());
    }

    @Test
    void pruneEmptyBranches() {
    }

    private MyrstackProjectFileNode createFakeFileRootNode() {
        return new MyrstackProjectFileNode(
                Paths.get("fake-root-dir"),
                Paths.get("fake-root-dir", "myrstack.yml")
        );
    }
}